import load_data as ld
import random
import re

spaces = re.compile('\s+')

def extract_random_chars(x_raw):
    new_x = []

    for sentence in x_raw:
        temp = spaces.sub('', sentence.lower())

        nbwords = len(sentence.split())
        nbchars = len(temp)
        nbsamples = min(2*nbwords, nbchars)

        chars = random.sample(temp, nbsamples)

        # Get 20 random letters from the set of characters
        new_x.append(' '.join(chars))

    return new_x


if __name__ == "__main__":

    X_raw = ld.load("./data/train_set_x.csv")

    new_x = extract_random_chars(X_raw)

    ld.write(new_x, './data/chars_train_set_x.csv','Id,Text')

    
