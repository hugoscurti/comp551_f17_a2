# coding: utf-8
import os
import random

from scipy import sparse
import numpy as np
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import sklearn.metrics

# User imports
import load_data as ld
import feature_extract
import transform


# In[ ]:

def nb_tfidf_bootstrap(n, y):
    paths = ['./data/chars_train_set_x'+str(k)+'.csv' for k in range(n)]
    bootstrap_preds=pd.DataFrame()
    test_preds=pd.DataFrame()
    for i in range(n):
        path = paths[i]
        best_a, best_preds, test_preds = nb_tfidf('tfidf_normal', y, x_path=path)
        bootstrap_preds[i] = best_preds
        test_preds[i]=test_preds
    bootstrap_preds['pred'] = bootstrap_preds.mode(axis=1)[0]
    return bootstrap_preds,test_preds




def nb_tfidf(sample, y, x_path):
       
    X_raw = ld.load(x_path)
    X_raw_test = ld.load('./data/test_set_x.csv')
    

    if(sample == 'tfidf_normal'):
        X, feat = feature_extract.tfidf_vectorize_letters(X_raw, 1, True)
        X_test = feature_extract.extract_chars_tfidf(X_raw_test, feat)
        
    elif(sample == 'normal'):
        X, feat = feature_extract.vectorize_letters(X_raw, 1, True)
        X_test = feature_extract.extract_chars_count(X_raw_test, feat)

    #build test/train, split size is 0.2. Transfer to pandas df for now, could optimize with a sparse
    xy = sparse.hstack((X, y)).A
    xy_train = xy[:int(xy.shape[0]*0.8),:]
    xy_test = xy[int(xy.shape[0]*0.8):,:]
    xy_train_pd = pd.DataFrame(xy_train)
    xy_test_pd = pd.DataFrame(xy_test)
    xy_train_pd.rename(columns={xy_train_pd.shape[1]-1 :'lang'}, inplace=True)
    xy_test_pd.rename(columns={xy_train_pd.shape[1]-1 :'lang'}, inplace=True)
    
    feat = [c for c in xy_train_pd.columns if c not in ['lang']]
    print('starting training')
    
    rf = RandomForestClassifier(n_estimators=300)
    rf.fit(X=xy_train_pd[feat], y=xy_train_pd['lang'])
    preds = rf.predict(xy_test_pd[feat])
    true = xy_test_pd['lang']
    acc = sklearn.metrics.accuracy_score(preds, true)
    test_preds = pd.DataFrame(rf.predict(X_test[feat]))
   
    return acc, preds, test_preds


# In[9]:

if __name__ == '__main__':

    #shuffle x_raw and y identically
    X_raw = ld.load("./data/train_set_x.csv")
    y = ld.load('./data/train_set_y.csv')
    c = list(zip(X_raw, y))
    random.shuffle(c)
    X_raw, y = zip(*c)

    # Construct 10 bootstrapped samples of characters from the original training sample
    for i in range(10):
        char_file = './data/chars_train_set_x{0}.csv'.format(i)
        if not os.path.isfile(char_file):
            new_x = transform.extract_random_chars(X_raw)
            ld.write(new_x, char_file,'Id,Text')

    y = np.array(y).reshape((len(y),1))
    df,test_preds = nb_tfidf_bootstrap(10, y)
    df.to_csv('best_preds_nb.csv')
    test_preds.to_csv('test_preds_bs_forest.csv')


