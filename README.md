# Project 2 : Language Classification #

COMP551 : Applied Machine Learning, Fall 2017

Authors :

- Hugo Scurti
- Baptiste Cumin
- Meixue Liu
- - -

This is the source code for our language classification project. This readme should help you delve into the project.

## Prerequisites ##

This project runs with __Python 3.6__. The project depends on the following libraries : 

- numpy
- scipy (=0.19.1) 
    - __Do not use version 1.0.0rc2, as dok_matrix.update seems to break__
- pandas
- scikit-learn
- matplotlib (only for "tfidf_models_notebook.ipynb")
- jupyter (only for "tfidf_models_notebook.ipynb")
- h2o (only for "tfidf_models_notebook.ipynb")


## Code ##

The code is mainly split in 3 parts : 

- Functions for preprocessing the input, doing feature selection and feature extractions
- Classifiers implemented ourselves, for categories 1 & 2 specified in the project description
- Classifiers used from libraries for category 3 from the project desciption, and a few experimentations on those

Below we first describe how to run the code using classifiers from categories 1 & 2, then describe how to run the code using classifiers from category 3.

### Methods 1 & 2 ###

#### Description ####
Here is a breakdown of files used for Methods 1 & 2, including the preprocessing and feature selection methods:

- folder **classifiers**: contains classes for naive bayes, decision tree and random forest classifiers. They typically use the same function signature (*train(X,y)*, *classify(X)*), with some exceptions (naive bayes also has a *classify_mutlithread* function to train using multiple threads)

- **load_data.py**: This file contains some utility functions to read from and write to csv files using the correct format, and a few functions for filtering out data from the training samples.

- **feature_extract.py**: This file contains the methods used to extract and select features, using words or characters as features, and using counts or tf-idf as feature values. We define a few options to filter out unnecessary features or features below a certain threshold.

- **training.py**: This is the entry point for training and testing methods 1 & 2.

#### Run code to analyze classifiers ####
To run analyze the performance on the classifiers developped in methods 1 & 2, use the following command :
```
    python training.py -analyze [-include-rf|-include-dt]
```
Using only ```-analyze``` will train a Naive Bayes with alpha 0.8 and tf-idf on characters as features, and will test it with a 5-fold validation on the training data, outputting the accuracy and confusion matrix.

Adding the optional parameter ```-include-rf``` will also train a random forest classifier with 100 trees and a sub feature splitting of the square root of the total number of features. __NOTE__: the random forest with 100 trees will take a very long time to train (approx. 4-10 hours)

Adding the optional parameter ```-include-dt``` will train the underlying decision tree classifier used by the random forest, with a maximum tree depth of 15. __NOTE__: the decision tree will take a significant time to train (approx. 2-3 hours)

#### Run on test data ####
To run one of the previously described classifiers on the test sample for the kaggle competition, use the following command : 
```
    python training.py -test (-nb|-rf|-dt)
```

Here, specifying ```-nb``` will use the naive bayes classifier, specifying ```-rf``` will use the random forest classifier, and specifying ```-dt``` will use the decision tree classifier.

This will output to a file *test_set_y.csv* in folder *results*.


### Method 3 ###

#### Description ####

Here is a breakdown of files used to implement classifiers for method 3 :

- **train_bootstrap_rf_tfidf.py**: This file trains 10 random forest classifier of 300 trees from **scikit-learn**, using 10 different sets of random character distributions from the training samples, averages the result of the 10 random forest classifiers.

- **train_one_vs_all.py**: This file uses **scikit-learn**'s random forest classifier (with 500 trees) in conjunction with a *OneVsRestClassifier* to train 5 different classifiers with a binary output (1 if it's the class specified, 0 otherwise)

- **train_rf_tfidf.py**: This file trains multiple random forest classifiers from **scikit-learn** with different number of trees and outputs the result of all of them.

- **tfidf_models_notebook.ipynb**: Jupyter notebook which experiments on multiple random forest classifiers from **scikit-learn** and **h2o**.

### How to run the files ###

For the 3 python files, simply run 
```
    python <filename>
```

For the jupyter notebook, run jupyter in a parent folder, and open the specified file in jupyter. 