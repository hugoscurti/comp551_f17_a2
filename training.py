import sys
import pickle
import numpy as np
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix

# User imports
import load_data as ld
import feature_extract as feat
import transform as xf
import classifiers.decision_tree as dt
import classifiers.naive_bayes as nb
import classifiers.random_forest as rf

def done(): 
    print("Done.\n")


def load_datasets(x_file, y_file):
    print("Loading data sets...")

    X_raw = ld.load(x_file)
    y = ld.load(y_file)

    done()
    return X_raw, y


def extract_features(X_raw, feat_type):

    print("Extracting features...")

    if feat_type == "tfidf":
        X, features = feat.tfidf_vectorize_letters(X_raw, 1, False)
    elif feat_type == "count":
        X, features = feat.vectorize_letters(X_raw, 1, False)

    done()

    # Since we are likely going to manipulate X by rows (when splitting), we return a csr copy
    return X.tocsr(), features


def get_features(x_file, y_file, feat_type="tfidf"):
    X_raw, y = load_datasets(x_file, y_file)
    X, features = extract_features(X_raw, feat_type)

    x_chars_raw = xf.extract_random_chars(X_raw)
    X_chars = get_test_features(x_chars_raw, features, feat_type)

    return X, X_chars, y, features


def get_test_features(x, features, feat_type="tfidf"):

    if isinstance(x, str):
        X_test_raw = ld.load(x)
    else:
        # Should be an array
        X_test_raw = x

    # Call different function for different types of features
    if feat_type == "tfidf":
        X_test = feat.extract_chars_tfidf(X_test_raw, features)
    else:
        X_test = feat.extract_chars_count(X_test_raw, features)

    return X_test


def real_train(classifier, X, y, features):

    print("Training classifier...")
    classifier.train(X, y)
    done()
    
    # Testing on the test file
    X_test = get_test_features('./data/test_set_x.csv', features)

    if hasattr(classifier, "classify_multithread"):
        print("Multithreaded classify...")
        y_res = classifier.classify_multithread(X_test, 4)
    else:
        print("Classifying...")
        y_res = classifier.classify(X_test)

    done()

    # Write result in csv file
    ld.write(y_res, './results/test_set_y.csv')


def test(classifier, X, y, X_chars):
    """Test one classifier on one split of the X set.
    Return the accuracy
    """

    nbsamples = X.shape[0]
    #Split into test and training
    train_pct = nbsamples // 5
    split = nbsamples - train_pct
    X_train = X[:split]
    X_test = X_chars[split:]

    y_train = y[:split]
    y_test = y[split:]

    print("Training classifier...")
    classifier.train(X_train, y_train)
    done()

    if hasattr(classifier, "classify_multithread"):
        print("Multithreaded classify...")
        y_res = classifier.classify_multithread(X_test, 4)
    else:
        print("Classifying...")
        y_res = classifier.classify(X_test)

    done()

    # Return the accuracy
    return (np.count_nonzero(y_res == y_test) / len(y_res)), y_res


def train_kfold(X, y, nb_folds, classifiers, X_chars):
    """Train a list of classifiers using k-fold validation
    and return the best classifier and the accuracy
    """

    kf = KFold(nb_folds, True)

    accuracies = np.zeros(len(classifiers))
    confusion_matrices = np.zeros((len(classifiers), 5, 5))
    kfold_idx = 1

    for train_idx, test_idx in kf.split(X):
        print("Training with kfold {0}...".format(kfold_idx))

        # Split into test and training
        X_train = X[train_idx]
        X_test = X_chars[test_idx]

        y_train = y[train_idx]
        y_test = y[test_idx]

        for i, classifier in enumerate(classifiers):
            if hasattr(classifier, 'train'):
                classifier.train(X_train, y_train)
                y_res = classifier.classify(X_test)
            else:
                classifier.fit(X_train, y_train)
                y_res = classifier.predict(X_test)

            accuracies[i] += (np.count_nonzero(y_res == y_test) / len(y_res)) / nb_folds
            confusion_matrices[i] = confusion_matrices[i] + confusion_matrix(y_test, y_res)

        done()
        kfold_idx += 1

    return accuracies, confusion_matrices


if __name__ == '__main__':

    X, X_chars, y, features = get_features('./data/fil_train_set_x.csv', './data/fil_train_set_y.csv')

    if len(sys.argv) >= 2 and sys.argv[1] == "-analyze":

        # Running 2 naive bayes and maybe one random forest
    
        classifiers = []
        classifiers.append(nb.NaiveBayes(0.8))

        if len(sys.argv) >= 3 and sys.argv[2] == "-include-rf":
            # Include random forest
            classifiers.append(rf.RandomForest(100, 0, 1.0, 'sqrt'))

        elif len(sys.argv) >= 3 and sys.argv[2] == "-include-dt":
            # Include decision tree
            classifiers.append(dt.DecisionTree(15, None))

        accuracies, conf_matrices = train_kfold(X, y, 5, classifiers, X_chars)
        
        print("Accuracies: {0}".format(accuracies))

        print("Confusion Matrices : {0}".format(conf_matrices))

    elif len(sys.argv) >= 3 and sys.argv[1] == "-test":

        if sys.argv[2] == "-nb":
            clssf = nb.NaiveBayes(0.8)
        elif sys.argv[2] == "-rf":
            clssf = rf.RandomForest(100, 0, 1.0, 'sqrt')
        elif sys.argv[2] == "-dt":
            clssf = dt.DecisionTree(15, None)

        real_train(clssf, X, y, features)

    else:
        print("Incorrect usage. Usage : python training.py (-analyze [-include-rf|-include-dt] | -test (-nb|-rf|-dt))")
