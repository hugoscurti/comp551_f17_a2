import pandas as pd
import codecs
import numpy as np
import re

def load(file):
    res = pd.read_csv(file, encoding='utf-8', keep_default_na=False)

    # Since the first column is exactly the same as the row number, we disregard it
    return res.values[:,1]

def write(data, file, header="Id,Category"):
    with codecs.open(file, 'w', encoding='utf-8') as f:
        f.write("{0}\n".format(header))
        for i,v in enumerate(data):
            f.write("{0},{1}\n".format(i,v))


def filter_data(X,Y):
    X_raw = load(X)
    Y_raw = load(Y)

    print('adding filter for repeated lines')
    sentences = filter_lines(X_raw, Y_raw)

    print('adding filter for non-word words')
    x, y = filter_words(sentences)

    print('writing data to new files')
    # Output the result into new files
    write(x, './data/fil_train_set_x.csv', 'Id,Text')
    write(y, './data/fil_train_set_y.csv', 'Id,Category')

def filter_lines(lines, labels):

    sentences= {}
    count = 0
    for line in lines:
        if line not in sentences or line == '':

            sentences[line] = labels[count]
        
        count = count + 1
    
    print('# of sentences before filter: ', count)
    print('# of sentences after filter:', len(sentences))

    return sentences


def filter_words(diction):
    text = []
    label = []
    for k, v in diction.items():
        words = k.split(' ')
        #print('ori sentence:', words)
        for word in words:
            number = re.findall('[0-9]+', word)
            if len(number) > 0: # find a word contains numbers and a number string
                #print('******************found:', word)
                words.remove(word) # then remove the word in the string
        k = ' '.join(words)
        text.append(k)
        label.append(v)
    #print('after filter: ', text, label)

    return text, label


# If this file is used as an import, this will not execute
if __name__ == "__main__":
    filter_data('./data/train_set_x.csv','./data/train_set_y.csv')