# coding: utf-8
from scipy import sparse
import sklearn.metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.multiclass import OneVsRestClassifier
import pandas as pd

# User imports
import load_data as ld
import feature_extract

def nb_tfidf(sample):
    X_raw_test = ld.load('./data/test_set_x.csv')

    if(sample in ['tfidf_normal', 'normal']):
        X_raw = ld.load('./data/fil_train_set_x.csv')
        y = pd.read_csv('./data/fil_train_set_y.csv')[['Category']].values

        if sample == 'tfidf_normal':
            X, feat = feature_extract.tfidf_vectorize_letters(X_raw, 1, True)
            X_test = feature_extract.extract_chars_tfidf(X_raw_test, feat)
        else:
            X, feat = feature_extract.vectorize_letters(X_raw, 1, True)
            X_test = feature_extract.extract_chars_count(X_raw_test, feat)
        
    elif(sample =='tfidf_sub_sample'):
        X_raw = ld.load('./data/chars_train_set_x.csv')
        y = pd.read_csv('./data/train_set_y.csv')[['Category']].values
        X, feat = feature_extract.tfidf_vectorize_letters(X_raw, 1, True)
        X_test = feature_extract.extract_chars_tfidf(X_raw_test, feat)

    
    #build test/train, split size is 0.2.
    xy = sparse.hstack((X, y)).A
    xy_train, xy_test = train_test_split(xy, shuffle=True, test_size=0.2)
    xy_train_pd = pd.DataFrame(xy_train)
    xy_test_pd = pd.DataFrame(xy_test)
    xy_train_pd.rename(columns={xy_train_pd.shape[1]-1 :'lang'}, inplace=True)
    xy_test_pd.rename(columns={xy_train_pd.shape[1]-1 :'lang'}, inplace=True)
    
    feat = [c for c in xy_train_pd.columns if c not in ['lang']]
    print('starting training')
    accs=[]
    
    nb = OneVsRestClassifier(RandomForestClassifier(n_estimators=500,max_depth=20, max_features=30))
    nb.fit(X=xy_train_pd[feat], y=xy_train_pd['lang'])
    preds = nb.predict(xy_test_pd[feat])
    true=xy_test_pd['lang']
    acc = sklearn.metrics.accuracy_score(preds, true)
    cm = sklearn.metrics.confusion_matrix(true, preds)

    # Test on the real test data
    test_preds = pd.DataFrame(nb.predict(X_test))
    test_preds.to_csv('test_preds.csv')

    return acc, cm, accs 


if __name__ == '__main__':
    acc, cm, accs = nb_tfidf('tfidf_sub_sample')
    print(acc, cm, accs)