# -*- coding: utf-8 -*-

import re
import numpy as np
import scipy.sparse as sp
from sklearn.feature_extraction.text import TfidfTransformer

# Regex that search for words. Disregard punctuations
word_regex = re.compile("[\w�']+")

# remove emojis
emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)

def vectorize_words(raw_X, min_freq):
    """ This function expects a column vector of sentences and a minimum frequency threshold 
    and proceed to transform into a matrix were columns are word features.
    Returns also the mapping of words and their indices in the matrix columns
    """
    
    # Store a dictionary of the features extracted with the name of the features
    columns = {}
    
    unigrams = []

    for sentence in raw_X:
        #remove emoji
        sentence = (emoji_pattern.sub(r'', sentence))
        sentence = sentence.lower()

        temp = word_regex.findall(sentence)
        
        # empty line as a feature (not sure if it's right)
        if len(temp) == 0:
            columns[''] = columns.get('', 0) + 1
        for unigram in temp:
            columns[unigram] = columns.get(unigram, 0) + 1
        
        #Append to the array
        unigrams.append(temp)

    # Filter out words smaller than the frequency threshold
    columns = [k for k,v in columns.items() if v > min_freq]
    col_idx = {val:idx for idx,val in enumerate(columns)}

    # Returns both the matrix and the feature mappings
    return extract_features(unigrams, col_idx), col_idx



def vectorize_letters(raw_X, min_freq, binary=False, remove_emojis=False):
    """ This function expects a column vector of sentences and a minimum frequency threshold 
    and proceed to transform into a matrix.
    """
    
    #noise_chars = [u'\u00AB', u'\u00BB', u'\u201c',u'\u2014', u'\u2026', u'\u22EF', u"\u221A", u'0', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9', u' ']
    # \u00AB « ; \u00BB	» ; \u201c " ; \u2014 — (long em dash); \u2026" ..., \u22EF ⋯, u221A √; 
    noise_chars = [u' ']
    # Store a dictionary of the features extracted with the name of the features
    letters = {}
    
    sentences = []

    for sentence in raw_X:
        #remove emoji
        if remove_emojis:
            sentence = emoji_pattern.sub(r'', sentence)

        temp = sentence.lower()

        if len(temp) == 0:
            letters[''] = letters.get('', 0) + 1
        else:
            for char in temp:
                letters[char] = letters.get(char, 0) + 1

        #Append to the array
        sentences.append(temp)

    # Filter out chars smaller than the frequency threshold
    features = [k for k,v in letters.items() if v > min_freq and k not in noise_chars]
    #features = [k for k,v in letters.items() if v > min_freq]

    col_idx = {val:idx for idx,val in enumerate(features)}
    

    # Returns both the matrix and the feature mappings
    return extract_features(sentences, col_idx, binary), col_idx


def tfidf_vectorize_letters(raw_X, min_freq, remove_emojis=False):
    # Version 1 with tfidftransform
    sparse_x, col_idx = vectorize_letters(raw_X, min_freq, False, remove_emojis)

    tfidf_transform = TfidfTransformer(norm=None, use_idf=True, smooth_idf=True)
    new_X = tfidf_transform.fit_transform(sparse_x)

    return new_X, col_idx


def extract_chars_count(raw_X, features):
    """Given a list of raw inputs, we preprocess them 
    and extract the specified features contained in the dictionary "features"
    """

    sentences = []
    for sentence in raw_X:
        #Append to the array
        sentences.append(sentence.lower())

    return extract_features(sentences, features)

def extract_chars_tfidf(raw_X, features):
    X_sparse = extract_chars_count(raw_X, features)

    tfidf_transform = TfidfTransformer(norm=None, use_idf=True, smooth_idf=True)
    new_X = tfidf_transform.fit_transform(X_sparse)

    return new_X


def extract_features(inputs, features, binary=False):
    """Given a list of preprocessed inputs (i.e. words or characters), 
    and a mapping of features that we want to keep,
    we build a matrix and count the occurences of features in the inputs
    """

    # Store values first in a standard dict for faster accessing
    vals = dict()

    # Count the number of occurences of each word and store it in X
    for i, val in enumerate(inputs):
        if not val:
            idx = features.get('', -1)
            if idx == -1 : continue

            if binary:
                vals[(i, idx)] = 1
            else:
                vals[(i, idx)] = vals.get((i,idx), 0) + 1

            
        for feat in val:
            idx = features.get(feat, -1)

            if idx == -1 : continue

            if binary:
                vals[(i, idx)] = 1
            else:
                vals[(i, idx)] = vals.get((i,idx), 0) + 1

    # We use scipy's sparse matrices instead
    # Since we store occurences, we specify the type integer
    X_matrix = sp.dok_matrix((len(inputs), len(features)), dtype='i')
    
    # Update the dok_matrix efficiently by passing the dictionary of values
    X_matrix.update(vals)

    return X_matrix