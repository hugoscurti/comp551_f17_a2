# coding: utf-8
import random
import numpy as np
from .decision_tree import DecisionTree


def _bootstrap_sample(x, y, n):
    """ Returns an x matrix with proportion n of observations.
    Note : features are NOT bootstrapped: we take a random subset. Only the observations are bootstrapped.
    """

    x_boot_bool = [0 for i in range(x.shape[0])] #tells us which observations are in current bootstrap
    x_boot = []
    y_boot = []
    
    n_bootstrap_obs = int(x.shape[0] * n)

    indices_boot = []
    
    #first, bootstrap observations
    for i in range(n_bootstrap_obs):
        i = random.randint(0, x.shape[0] - 1)
        indices_boot.append(i)
        x_boot_bool[i] += 1

    # Get bootstraped x and y
    x_boot = x.tocsr()[indices_boot,:]
    y_boot = y[indices_boot]
    
                    
    return x_boot, y_boot, x_boot_bool

class RandomForest:

    def __init__(self, n_trees=5, max_depth=15, boot_obs=0.7, sub_feats="sqrt"):

        # List of DecisionTree objects
        self.trees = []
        self.n_trees = n_trees
        self.max_depth = max_depth
        self.boot_obs = boot_obs
        self.sub_feats = sub_feats


    def train(self, X, y):
        self.trees.clear()
    
        for i in range(self.n_trees):
            #get bootstrapped sample
            # x and boot_x are sparse matrices
            boot_x, boot_y, x_boot_bool = _bootstrap_sample(X, y, self.boot_obs)
            print("boostrapped new sample: ", str(boot_x.shape))
        
            #build decision tree, include information on features in tree[0].
            tree = DecisionTree(self.max_depth, sub_feats=self.sub_feats)
            tree.train(boot_x, boot_y)
            print("tree ", i, " built")
        
            # Appends the tree to the array
            self.trees.append(tree)
        


    def classify(self, X):
        preds = [dict() for n in range(X.shape[0])]

        for t in self.trees:

            #subset features
            cur_preds = t.classify(X)
        
            #vote for each predicted obs
            for i,k in zip(cur_preds, range(len(cur_preds))):
                if i in preds[k].keys():
                    preds[k][i] += 1
                else:
                    preds[k][i] = 1
                       
    
        #find maximum of votes
        for i in range(len(preds)):
            preds[i] = max(preds[i].keys(), key=(lambda k: preds[i][k]))
        
        return preds

