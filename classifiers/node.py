
class Node:
    """description of class"""

    def __init__(self, feature_idx, class_val, left_child=None, right_child=None):
        # Tree values
        self.feature_idx = feature_idx
        self.class_val = class_val
        # Subtrees
        self.left = left_child
        self.right = right_child

