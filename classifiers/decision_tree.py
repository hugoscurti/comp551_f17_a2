import numpy as np
import scipy.stats as stats
import math
import random

# User imports
from .node import Node

def _info_gain(x_col_coo, y_data, split, total_entropy, count):
    """Return the information gain by splitting the ys
    at the split value. x_left <= split, x_right > split.
    Therefore, all zero values in the sparse matrix are automatically on the left.
    x_col_coo is expected to be a COO sparse matrix
    """
    if count == 0: return 0 # ??

    # This is MUCH FASTER than iterating through indices in a csc sparse matrix and querying the value at the index
    right_mask = np.zeros_like(y_data, dtype=bool)
    for row, val in zip(x_col_coo.row, x_col_coo.data):
        if val > split:
            right_mask[row] = True

    #right_mask[left_indices] = False

    y_left = y_data[~right_mask]
    y_right = y_data[right_mask]
    
    left_entropy = _entropy(y_left) * (y_left.shape[0] / count)
    
    right_entropy = _entropy(y_right) * (y_right.shape[0] / count)
    
    return total_entropy - right_entropy - left_entropy


def _entropy(y_vals):
    """Calculate the entropy of the values passed in parameters
    """

    val, counts = np.unique(y_vals, return_counts=True)

    if len(val) < 2 or len(y_vals) == 0: return 0.0
    
    # get probabilities of each value
    probs = counts / float(len(y_vals))
    
    total_entropy = 0.0
    for p in probs:
        total_entropy -= math.log(p, 2) * p
    
    return total_entropy


def _split_data(x_data, y_data, feat_iterator):
    #takes x and y as input, returns the indices of observations for the left branch and for the right branch + info on the split.
    top_feat_ig = (0, 0, 0)
    #the elements of top_feat_ig are: (info_gain, feature_id, split)
    x_data_csc = x_data.tocsc()

    # Calculate total entropy in advance
    total_entropy = _entropy(y_data)
    total_count = float(x_data.shape[0])

    for i in feat_iterator:
        feat_x = x_data_csc[:,i].tocoo()
        unique_x = np.insert(np.unique(feat_x.data), 0, 0)
        #unique_x = np.unique(feat_x)

        for j,val in enumerate(unique_x):
            # Split only by condition feature > value
            info_gain_split = _info_gain(feat_x, y_data, val, total_entropy, total_count)
            
            if info_gain_split > top_feat_ig[0]:
                top_feat_ig = (info_gain_split, i, val)

            if j < len(unique_x) - 1:
                val_ext = val + unique_x[j+1] / 2.0
                info_gain_split = _info_gain(feat_x, y_data, val_ext , total_entropy, total_count)
                if info_gain_split > top_feat_ig[0]:
                    top_feat_ig = (info_gain_split, i, val_ext)
                    
    # Split into left and right
    split = top_feat_ig[2]
    feature = top_feat_ig[1]
    i_left = []
    i_right = []
    for e in range(x_data.shape[0]):
        if x_data[e,feature] <= split:
            i_left.append(e)
        else:
            i_right.append(e)   # different than split
   

    return i_left, i_right, top_feat_ig


class DecisionTree:
    """Decision tree classifier object"""

    def __init__(self, max_depth=15, sub_feats=None):
        self.tree = None
        self.max_depth = max_depth
        self.sub_feats = sub_feats


    def _get_sub_feats(self, M):
        """return a list of indices which, by the value of self.sub_feats, 
        is a subset of M, the number of features
        """
        if self.sub_feats is not None:
            if self.sub_feats == "sqrt":
                # We take the square root of the number of features
                m = int(math.sqrt(M))
            else:
                # Choose split on a subset of the features
                m = int(M * self.sub_feats)

            feat_idxs = random.sample(range(M), m)
        else:
            feat_idxs = range(M)

        return feat_idxs


    def _build_decision_tree(self, x_data, y_data, depth):
        """builds a decision tree, storing the data as a list of splits. 
        list[1]= first split, list[2]=left split to first split, list[3]=right split. i*2=left child, (i*2)+1=right child.
        split represented as (feature, split_value). if we're at a leaf, returns (-1, prediction)
        """
        #if all obs are same, return leaf node and prediction 
        if len(np.unique(y_data))==1:
            return Node(-1, y_data[0])
    
        #if max depth reached, return a leaf with a vote.
        elif self.max_depth > 0 and depth >= self.max_depth:
            return Node(-1, stats.mode(y_data)[0][0])

        feat_idxs = self._get_sub_feats(x_data.shape[1])
        
        left, right, split = _split_data(x_data, y_data, feat_idxs)

        if split[0] == 0:
            # No splitting found
            return Node(-1, stats.mode(y_data)[0][0])

        #build decision tree for left data
        left_x = x_data[left,:]
        left_y = y_data[left]
        tree_left = self._build_decision_tree(left_x, left_y, depth=depth+1)
    
        #build decision tree for right data
        right_x = x_data[right,:]
        right_y = y_data[right]
        tree_right = self._build_decision_tree(right_x, right_y, depth=depth+1)
    
        #combine left, right into 1 tree
        tree = Node(split[1], split[2], tree_left, tree_right)
        
        #return tree
        return tree

    def train(self, X, y):
        self.tree = self._build_decision_tree(X, y, 0)


    def classify(self, X):
        """Given the tree constructed while training, 
        returns a vector y of predictions on the same index as the features in X. 
        Uses voting.
        x_input is a sparse matrix
        """
    
        y_preds = np.zeros(X.shape[0], dtype='i')
    
        #currently doing the parsing from the tree observation by observation. Must be a faster way to do this.
        for k in range(X.shape[0]):
            node = self.tree

            while node.left is not None and node.right is not None:
                xval = X[k, node.feature_idx]
                
                if xval <= node.class_val:
                    node = node.left
                else:
                    node = node.right
            
        
            y_preds[k] = int(node.class_val)
    
        return y_preds


