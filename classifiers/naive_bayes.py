# -*- coding: utf-8 -*-

import numpy as np
import math
from multiprocessing.dummy import Pool as ThreadPool

class NaiveBayes:
    
    # We could initiate hyperparameters here eventually, if necessary
    def __init__(self, alpha=0.0, log_space=True):

        # Alpha parameter for smoothing (if alpha == 1 then laplace smoothing)
        self.alpha = alpha
        self.log_space = log_space

        # Declare prob_x and prob_y here
        self.prob_x = None
        self.prob_y = None

    def train(self, X, y):
        """Compute probabilities of all features in X
        and propbabilities of every class y and store for predicting
        """
        # Use coo_matrix for iterating over all values
        X_coo = X.tocoo()

        # Get all different classes of y
        class_count = float(len(y))
        temp_vals = np.unique(y,return_counts=True)

        # We assume the classes of y are from 0 to i, and every row of prob_y represents the class with the same value of its index
        self.prob_y = np.zeros(np.max(y) + 1)    # Since indexing is done from 0, the shape should be the max index + 1
        self.prob_y[temp_vals[0]] = temp_vals[1] / class_count

        # Calculate likelihoods of each feature x given y
        # Rows represents classes with the same index
        # Columns represents the words in the vocabulary (i.e. features)
        # Cell(i,j) represents the probability of feature j given the class i
        # To use the smoothing parameter alpha, we simply instatiate matrix X with the constant alpha
        # and vector y with the constant alpha*n instead of initializing them to 0
        frequ_x = np.ones((self.prob_y.shape[0], X.shape[1])) * self.alpha
        frequ_y = np.ones(self.prob_y.shape) * (self.alpha * X.shape[1])  # alpha * number of features

        # Iteration over a triplet of values from a COO sparse matrix
        for row, col, val in zip(X_coo.row, X_coo.col, X_coo.data):
            y_class = y[row]

            # Computes both the matrix of frequencies and the vector of words per class at the same time
            frequ_y[y_class] += val
            frequ_x[y_class, col] += val

        # Accumulate the probabilities by dividing the frequency of each words per class over the total words for a class
        # Using frequ_y[:,None] forces the vector frequ_y to be vertical, so that it divides prob_x along the 
        self.prob_x = frequ_x / frequ_y[:,None]

        return


    def _classify(self, x):
        """Classify one row of data given the currently trained classifier.
        Returns a single value, the class.
        x is expected to be a numpy 1 * n matrix
        """

        #Reset the max for this sample
        max_prob = float("-inf") if self.log_space else 0

        winning_class = -1

        # Iterate over all classes
        for j in range(self.prob_y.shape[0]):
            # We calculate the probability of y given x for every feature 
            # using P(y|x) = P(y)* P(x_i)^freq_x for all features i
            # We don't divide by P(x) since P(x) is the same for every class. 
            # Therefore, the best class will still be better than the other even without dividing by P(x)
            
            if self.log_space:
                # Using log-space
                # Log space of probabilities will always be negative, 
                # and the biggest value (i.e. the smallest absolute value) wins
                temp_prob = math.log(self.prob_y[j],2) + np.sum(np.multiply(np.log2(self.prob_x[j]), x))
            else:
                # Not using log-space
                temp_prob = self.prob_y[j] * np.prod(self.prob_x[j] ** x)


            # If better than max_prob, set as the current winning class
            if temp_prob > max_prob:
                max_prob = temp_prob
                winning_class = j

        #Set the winning class in the array of results y
        return winning_class


    def classify(self, X):
        """Classify the data X given the training done in train(X,y)
        returns an array of size X.shape[0] representing the predicted class for every sample

        X is expected to by a sparse matrix
        """
        
        # Ultimate pythonic one-liner
        return [self._classify(x.todense()) for x in X]


    def classify_multithread(self, X, nbthreads):
        """Multithread version of the classify function
        """
        pool = ThreadPool(nbthreads)
        res = pool.map(self._classify, X.todense())

        #close the pool and wait for the work to finish 
        pool.close()
        pool.join()

        return res


    def get_n_best_features(self, n, features):
        """Returns the index of the n best features for each class
        rows are the classes, columns are the features
        """
        temp_res = np.array([r.argsort()[-50:][::-1] for r in self.prob_x])

        letters = np.zeros(len(features), dtype="U")
        for k,v in features.items():
            letters[v] = k
        
            res = []
        # Create a matrix of tuples
        for i in range(temp_res.shape[0]):
            res.append([(letter, freq) for letter,freq in zip(letters[temp_res[i]], self.prob_x[i,temp_res[i]])])

        return res
        
